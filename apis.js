
const Firestore = require('@google-cloud/firestore');
const db = new Firestore({
    projectId: 'proven-grin-253204',
    keyFilename: `${process.cwd()}/proven-grin-253204-81da40afc231.json`,
});
const stream = require('stream')
const express = require('express')
//bucket
const { Storage } = require('@google-cloud/storage');
const storage = new Storage({
    projectId: 'proven-grin-253204',
    keyFilename: `${process.cwd()}/proven-grin-253204-81da40afc231.json`,
});
const bucket = storage.bucket('upload-files-sertis');

const getSecret = (req, res) => {
    res.json({ "message": "THIS IS SUPER SECRET, DO NOT SHARE!" })
}

const collectionName = "cards"

let REDIS_PORT = parseInt(process.env.REDIS_PORT) || 6379;
let REDIS_HOST = process.env.REDIS_HOST || 'localhost';

let REDIS_URL = `redis://${REDIS_HOST}:${REDIS_PORT}`;

var redis = require('redis');
var rClient = redis.createClient(REDIS_URL); //creates a new client



const uploadFile = async (req, res, next) => {
    try {
        var options = {
            metadata: {
                name: req.file.originalname
            }
        };

        let myfile = await bucket.upload(req.file.path, options)
        if (!myfile) {
            console.log("cannot upload");
            res.status(500).json({ 'error': "cannot upload" })
        }
        else {
            //  console.log(myfile.file.filename);
            res.sendStatus(200)
        }

    } catch (error) {
        res.status(500).json(error)

    }
}

const downloadFile = async (req, res, next) => {
    try {
        const localFilename = './download/' + `${req.params.fileName}`;

        let myfile = await bucket.file(req.params.fileName).download();
        res.set('Content-Type', 'image/jpeg')
        let bufferStream = new stream.PassThrough()
        if (myfile != null) {
            bufferStream.end(myfile[0])
        }
        bufferStream.pipe(res)


    } catch (error) {
        //console.log(error)
        res.status(500).json(error)
    }
}

const listFiles = async (req, res, next) => {
    try {
        let array = await bucket.getFiles()
        let allnames = []
        if (array != null) {
            array[0].forEach(name => {
                allnames.push(name.id)
            })
        }
        console.log('list all file')
        res.status(200).json(allnames)
    }
    catch (error) {
        console.log("error")
        res.status(500).json(error)
    }
}

const getData = async (req, res, next) => { // get specific cards

    try {
        let result = await db.collection("cards").doc(`${req.params.cardId}`).get()
        res.sendStatus(200) //response 
    }
    catch (error) {
        res.sendStatus(500)
    }
}

const createData = async (req, res, next) => {
    try {
        let result = await db.collection("cards").doc(`${req.body.cardId}`).set(req.body)
        console.log("1 document added")
        res.status(200).json(result) //response 
    }
    catch (error) {
        console.log("no document added due to an error")
        res.status(500).json(error)
    }

}

const updateData = async (req, res, next) => {
    try {
        let result = await db.collection("cards").doc(`${req.params.cardId}`).update(req.body)
        console.log("1 document updated");
        res.status(200).json(result) //response 
    }
    catch (error) {
        // console.log(error)
        res.sendStatus(500)
    }
}

const deleteData = async (req, res, next) => {
    try {
        let obj = await db.collection("cards").doc(`${req.params.cardId}`).delete()
        console.log("1 document deleted");
        res.status(200).json(obj) //response 
    }
    catch (error) {
        res.sendStatus(500)
    }
}

// create data to redis
const rcreateData = async (req, res, next) => {
    await rClient.set(req.body.Name, req.body.keyValue, function (err, reply) {
        try {
            console.log(reply)
            res.status(200);

        } catch{
            console.log(err)
            res.status(500).json(err);
        }
    });
}


module.exports = {
    getSecret, //
    uploadFile,//
    downloadFile,//
    createData,//
    updateData,//
    deleteData,//
    listFiles,//
    getData,//
    rcreateData
}