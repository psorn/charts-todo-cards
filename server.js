
const Firestore = require('@google-cloud/firestore');

const db = new Firestore({
    // projectId: process.env.project-id,
    // keyFilename: `/etc/foo/proven-grin-253204-81da40afc231.json`,
    projectId: 'proven-grin-253204',
    keyFilename: `${process.cwd()}/proven-grin-253204-81da40afc231.json`,
});
const apis = require('./apis')
// JWS
const jwt = require('jsonwebtoken');
const fs = require('fs')
//express
const express = require('express')
const app = express()
const port = 3000

var bodyParser = require('body-parser')
app.use(bodyParser.json())
var empty = false
var multer = require('multer')
var mStorage = multer.diskStorage(
    {
        destination: './uploads/',
        filename: filename

    }
);
const stream = require('stream')

var upload = multer({ storage: mStorage });
//bucket
const { Storage } = require('@google-cloud/storage');
const storage = new Storage({
    // projectId: process.env.project - id,
    projectId: 'proven-grin-253204',
    keyFilename: `${process.cwd()}/proven-grin-253204-81da40afc231.json`,
    // keyFilename: `/etc/foo/proven-grin-253204-81da40afc231.json`,
});
const bucket = storage.bucket('upload-files-sertis');

let REDIS_PORT = parseInt(process.env.REDIS_PORT) || 6379;
let REDIS_HOST = process.env.REDIS_HOST || 'localhost';

let REDIS_URL = `redis://${REDIS_HOST}:${REDIS_PORT}`;

var redis = require('redis');
var rClient = redis.createClient(REDIS_URL);

// Use connect method to connect to the server

rClient.on('connect', function() {
    console.log('redis connected');
});

/// mongo, mongoose
const mongoose = require('mongoose');

mongoose.connect('mongodb://mongo:27017/test', {useNewUrlParser: true}) .then(() => {
    console.log('Database connection successful')
  })
  .catch(err => {
    console.error('Database connection error')
  })


app.get('/healthz', (req, res) => {
    res.status(200).json({"message": "Health check passed"})
})

app.get('/', (req, res) => {
    res.status(200).json({"message": "...."})
})

app.post('/mongoCreate', async(req,res,next)=>{
    try{
        console.log("in")
        const Cat = mongoose.model('Cat', { name: String });
        const kitty = new Cat({ name: 'Zildjian' });

        await kitty.save().then(doc => {
            console.log(doc)
            res.status(200).json({doc})
        })
        .catch(err => {
            console.error(err)
            res.status(500).json({err})
        })
    }catch{
        res.status(500).status(error)
    }
    

})

/////////////////////////////////////////////////////////
let cards = [];

function filename(req, file, cb) {

    //req.body is empty...
    //How could I get the new_file_name property sent from client here?
    cb(null, file.originalname);
}


function isAuthenticated(req, res, next) {
    try {
        if (typeof req.headers.authorization !== "undefined") {

            // retrieve the authorization header and parse out the
            // JWT using the split function
            let token = req.headers.authorization.split(" ")[1];
            let privateKey = fs.readFileSync('./private.pem', 'utf8');
            // Here we validate that the JSON Web Token is valid and has been 
            // created using the same private pass phrase
            let result = jwt.verify(token, privateKey, { algorithm: "HS256" })
            return next()

        }
        else {
            res.status(401).json({ error: "Not Authorized" });
        }
    } catch (error) {
        res.status(401).json({ error: "Not Authorized" });
    }

}

async function checkFileName(req, res, next) {

    try {
        let files_from_bucket = await bucket.getFiles()
        let all_filename = [];

        if (files_from_bucket.length) {
            files_from_bucket[0].forEach(name => {
                if (name.id == req.file.name) {
                    res.status(409).json({ name: "file exist." })
                }
            })
        }

        return next()
    }
    catch (error) {
        res.status(500).json(error)
    }

}

// create data to redis
app.post('/rcreate', apis.rcreateData, (req, res) => {

})

// let's first add a /secret api endpoint that we will be protecting
app.get('/secret', isAuthenticated, apis.getSecret)

// endpoint for jwt

app.post('/login', (req, res) => {
    let password = req.body.password
    if (password === '123456') {
        let privateKey = fs.readFileSync('./private.pem', 'utf8');
        let token = jwt.sign({ "body": "stuff" }, privateKey, { algorithm: 'HS256' });
        res.json({ token });
    } else res.sendStatus(401)

})

// upload file
app.post('/uploadFile', upload.single('file'), checkFileName)

//download file
app.get('/download/:fileName', apis.downloadFile)

//list all files to user
app.get('/get', apis.listFiles)

//get
app.get('/:cardId', apis.getData)

//create
app.post('/', isAuthenticated, apis.createData)

//update
app.put('/:cardId', apis.updateData)

//delete
app.delete('/:cardId', apis.deleteData)

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

// process.on('SIGINT', function () {

//     process.exit(0)
// })

module.exports = {
    isAuthenticated,
    checkFileName,
    filename

}